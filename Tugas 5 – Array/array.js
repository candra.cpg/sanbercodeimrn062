//Soal No. 1
function range(startNum, finishNum) {
  
   var res = -1;
   var val = [];
  if ((startNum!=null)&(finishNum!=null)){      
    if (startNum<finishNum){
        for (var i=startNum;i<=finishNum;i++){
            val.push(i);
        }        
    } else {
        for (var i=startNum;i>=finishNum;i--){
            val.push(i);
        }
    }
    res = val;  
  } 
  return res;
} 

console.log("No. 1")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


//Soal No. 2
function rangeWithStep(startNum, finishNum, step) {
  
    var res = -1;
    var val = [];
    if ((startNum!=null)&(finishNum!=null)){      
        
        if (startNum<finishNum){           
            var i=startNum-step;                   
            while ((i+step)<=finishNum){
                i = i+step;
                val.push(i);            
            }
        } else {            
            var i=startNum+step;    
            while ((i-step)>=finishNum){
                i = i-step;
                val.push(i);            
            }
        }     
        res = val;  
   } 
   return res;
 } 
console.log("")
console.log("No. 2")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


//Soal No. 3
function sum(startNum, finishNum, step) {
  
    var res = 0;
    var val = 0;
    if (step==null){
        step=1;
    }
    if (startNum==null){
        res = 0;
    } else if (finishNum==null){
        res = startNum
    } else {                
        if (startNum<finishNum){           
            var i=startNum-step;                   
            while ((i+step)<=finishNum){
                i = i + step;
                res = res + i;                                
            }
        } else {            
            var i=startNum+step;    
            while ((i-step)>=finishNum){
                i = i-step;
                res = res + i;          
            }
        } 
                  
   } 
   return res;
 } 
console.log("")
console.log("No. 3")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


//Soal No.4
function dataHandling(inputArr){
    for (var arrIdx in inputArr) {
        console.log("Nomor ID: "+ inputArr[arrIdx][0]);
        console.log("Nama Lengkap: "+ inputArr[arrIdx][1]);
        console.log("TTL: "+ inputArr[arrIdx][2]);
        console.log("Hobi: "+ inputArr[arrIdx][3]);
        console.log("");
    }
}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
console.log("")
console.log("No. 4")
dataHandling(input)


//Soal No.5
function balikKata(p_input){
    var j=0;
    var i =p_input.length;
    
    if (i==j){
        return p_input;
    } else {
        var res = ""
        while (i>j){
            res = res+p_input[i-1];
            i--;
        }
        return res;
    }
}
console.log("")
console.log("No. 5")
console.log(balikKata("Kasur Rusak"))// kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 