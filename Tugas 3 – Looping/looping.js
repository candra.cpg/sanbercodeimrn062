
// Soal No. 1
console.log("No. 1");
console.log("LOOPING PERTAMA");
var deret = 1;
while (deret <= 20){

    if (deret % 2 == 0){
        console.log(String(deret)+" - I love coding");
    }    
    deret++;

}
console.log("LOOPING KEDUA");
while (deret >0 ){

    
    if (deret % 2 == 0){

        console.log(String(deret)+" - I will become a mobile developer");

    }    
    deret--;
}


// Soal No. 2
console.log("");
console.log("No. 2");
for(var angka = 1; angka <= 20; angka++) {
    if ((angka%3==0) && (angka%2!=0)){
        console.log(angka + ' - I Love Coding');
    }else if (angka%2==0){
        console.log(angka + ' - Berkualitas');
    }else {
        console.log(angka + ' - Santai');
    }    
} 

// Soal No. 3
console.log("");
console.log("No. 3");
var baris = 1;
while (baris<=4){
    var pagar = "#";
    for (var i = 1; i <=8; i++) {
        pagar = pagar+"#";
    }
    console.log(pagar);
    baris++;
}

// Soal No. 4
console.log("");
console.log("No. 4");
baris = 1;
while (baris<=7){    
    pagar = "#";
    for (var i = 1; i < baris; i++) {
        pagar = pagar+"#";        
    }
    console.log(pagar);
    baris++;
}

// Soal No. 5
console.log("");
console.log("No. 5");

var baris = 1;
var j = 1;
while (baris<=8) {    
    pagar = "";
    for (var i=1; i<=8; i++){        
        if (j%2==0){
            pagar = pagar+"#";
        } else {
            pagar = pagar+" ";
        }        
        j++;
    }     
    j++;
    console.log(pagar);
    baris++;
}