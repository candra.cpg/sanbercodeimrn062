var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let i = 0;
let nRead=10000;
function read() {    
    let finish = false;
    if((i < books.length)&&(!finish)){
        if (nRead < books[i]['timeSpent']) {
            finish = true;
        } else {
            readBooks(nRead, books[i], function(x){})
            nRead = nRead - books[i]['timeSpent'];
            setTimeout( read, books[i]['timeSpent'] );
            i++;
            if ((nRead>books[0]['timeSpent']) && (i==books.length)){
                i=0
            }
        }
        
    }
    
}
read();

