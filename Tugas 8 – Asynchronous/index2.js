var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

function timer(ms) { return new Promise(res => setTimeout(res, ms)); }

async function longRead(longRead_) {
    await timer(longRead_);    
}

async function read(nRead) {
    finish = false;
    i=0;
    while (!finish){
        if (i==books.length){
            i=0;
        }
        if (nRead<books[i]['timeSpent']){
            finish = true
        } else {
            readBooksPromise(nRead, books[i])
            firstRead = books[i]['timeSpent'] 
            nRead = nRead - books[i]['timeSpent'];
            i++;
            await longRead(firstRead);        
        }
    }
}
read(10000);