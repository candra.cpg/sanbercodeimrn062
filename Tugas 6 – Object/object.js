// SOAL NO. 1
function arrayToObject(arr) {
    // Code di sini 
    if (arr.length==0){
        return "";
    } else {
        let res="";
        let i = 1;
        let personObj = {}
        arr.forEach(function(row){
            var strAge = row[3];
            if ((row[3]==null)||(row[3]>2020)){
                strAge = "Invalid Birth Year";
            }    
            
            personObj.firstName = row[0];
            personObj.lastName = row[1];
            personObj.gender = row[2];
            personObj.age = strAge;
            console.log(i+". "+row[0]+" "+row[1]+": " , personObj);    
            i++;    
            
        });
        
    }
}
 
// Driver Code
console.log("No. 1");
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
arrayToObject([]) 


// SOAL NO. 2
function shoppingTime(memberId, money) {
    // you can only write your code here!
    
    if ((memberId==="") || (money===0) || (money==null)){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"; 
    } else {
        
        let listPurchased = []
        let objMember = {}
        let sisa = money;
        let i = 0;
        let isFound = false;        
        let item = [ ["Sepatu", "Stacattu", 1500000], ["Baju", "Zoro", 500000], ["Baju", "H&N", 250000], ["Sweater", "Uniklooh", 175000], ["Casing", "Handphone", 50000]];            
        item.forEach(function(row){
            if ((sisa-row[2])>=0){
                listPurchased[i] = row[0]+" "+row[1];
                sisa=sisa-row[2];
                i++;
                isFound = true;
            }
        });
        if (!isFound){
            return "Mohon maaf, uang tidak cukup"
        } else {
            objMember.memberId = memberId;
            objMember.money = money;
            objMember.listPurchased = listPurchased;
            objMember.changeMoney = sisa;

            return objMember;
        }
    }
}

  // TEST CASES
  console.log("");
  console.log("No. 2");
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
  console.log(shoppingTime('82Ku8Ma742', 170000));
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
  


  //SOAL NO. 3
  function naikAngkot(arrPenumpang) {
    //your code here
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    let res = [];
    arrPenumpang.forEach(function(row){       
        let objPassenger = {};  
        row.forEach(function(col, i, array){                      
            objPassenger.penumpang = array[0];
            objPassenger.naikDari = array[1];
            objPassenger.tujuan = array[2];    
            objPassenger.bayar = 2000*rute.slice(rute.indexOf(array[1]), rute.indexOf(array[2])).length;
                           
        }); 
        res.push(objPassenger);                
    });
    return res;
  }
   
  //TEST CASE
  console.log("");
  console.log("No. 3");
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]