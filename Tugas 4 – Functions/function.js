//Soal No. 1
function teriak(){
    return "Halo Sanbers!";
}
console.log("No.1"); 
console.log(teriak());

//Soal No. 2
function kalikan(param1 , param2){
    return param1 * param2;
}
var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(""); 
console.log("No.2"); 
console.log(hasilKali);

// Soal No. 3
function introduce(p_name, p_age, p_address, p_hobby){
    return "Nama saya "+p_name+", umur saya "+p_age+" tahun, alamat saya di "+p_address+", dan saya punya hobby yaitu "+p_hobby+"!";
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby);
console.log(""); 
console.log("No.3");
console.log(perkenalan);