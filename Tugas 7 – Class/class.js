
// Soal Release 0
class Animal {
    constructor(animalName) {
        this.name = animalName;
        this.legs = 4;
        this.cold_blooded = false;
    }    
}
 
var sheep = new Animal("shaun");
console.log("Release 0");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


//Soal Release 1
class Ape extends Animal{
    constructor(animalName) {
        super(animalName);
        this.sound = "Auooo";
    }
    yell(){
        console.log(this.sound);
    }
}

class Frog extends Animal{
    constructor(animalName) {
        super(animalName);
        this.motion = "hop hop";
    }
    jump(){
        console.log(this.motion);
    }
}
console.log("");
console.log("Release 1");
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


// Soal No. 2


class Clock {
    constructor({template}){
        this.template= template;
        this.timer;
    }

    render() {
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs);
    
        console.log(output);
    }

    stop() {
        clearInterval(this.onTimer);
    }

    start() {
        this.timer = setInterval(() => this.render(), 1000);
    }
}
console.log("");
console.log("Soal No. 2");
var clock = new Clock({template: 'h:m:s'});
clock.start(); 
