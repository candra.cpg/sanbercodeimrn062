// Soal No. 1
var nama = "John";
var peran = "Werewolf";

console.log("No. 1");
if (nama==""){
    console.log("Nama harus diisi!");
}else if (peran==""){
    console.log("Halo "+nama+", Pilih peranmu untuk memulai game!");
}else if (peran==="Penyihir"){
   console.log("Selamat datang di Dunia Werewolf, "+nama); 
   console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
}else if (peran==="Guard"){
    console.log("Selamat datang di Dunia Werewolf, "+nama); 
    console.log("Halo Penyihir "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
}else if (peran==="Werewolf"){
    console.log("Selamat datang di Dunia Werewolf, "+nama); 
    console.log("Halo Werewolf "+nama+", Kamu akan memakan mangsa setiap malam!");
}


// Soal No. 2
var hari = 12; 
var bulan = 9; 
var tahun = 1945;
var strBulan = "";

console.log("");
console.log("No. 2");
if ((hari<1) || (hari>31)){
    console.log("Isian hari harus antara 1 sampai dengan 31");
} else if ((bulan<1) || (bulan>12)){
    console.log("Isian bulan harus antara 1 sampai dengan 12");
} else if ((tahun<1900) || (tahun>2200)){
    console.log("Isian bulan harus antara 1900 sampai dengan 2200");
} else {
    switch (bulan){
        case 1 : strBulan = "Januari"; break;
        case 2 : strBulan = "Februari"; break;
        case 3 : strBulan = "Maret"; break;
        case 4 : strBulan = "April"; break;
        case 5 : strBulan = "Mei"; break;
        case 6 : strBulan = "Juni"; break;
        case 7 : strBulan = "Juli"; break;
        case 8 : strBulan = "Agustus"; break;
        case 9 : strBulan = "September"; break;
        case 10 : strBulan = "Oktober"; break;
        case 11 : strBulan = "November"; break;
        case 12 : strBulan = "Desember"; break;
    }
    console.log(String(hari)+' '+strBulan+ ' '+String(tahun)); 
}